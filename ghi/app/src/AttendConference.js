import React, { useEffect, useState } from 'react';


function AttendConferenceForm () {
    const [conferences, setConferences] = useState([]);

    const [formData, setFormData] = useState({
        email: "",
        name: "",
        conference: ""
    })

    const handleNameChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setConferences(data.conferences)
            // console.log(conferences)
            } else {
                console.log("i'm broken")
            }
        }

    useEffect(() => {
        fetchData();
        }, []);

    // console.log(conferences)
    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log(formData)

        const conferenceUrl = 'http://localhost:8001/api/attendees/';
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                setFormData({
                    email: "",
                    name: "",
                    conference: ""
                })

            }
        };


    return (
    <div className="my-5 container">
    <div className="row">
      <div className="col col-sm-auto">
        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"></img>
      </div>
      <div className="col">
        <div className="card shadow">
          <div className="card-body">
            <form onSubmit={handleSubmit} id="create-attendee-form">
              <h1 className="card-title">It's Conference Time!</h1>
              <p className="mb-3">
                Please choose which conference
                you'd like to attend.
              </p>
              <div className="d-flex d-none justify-content-center mb-3" id="loading-conference-spinner">
                <div className="spinner-grow text-secondary" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              </div>
              <div className="mb-3">
                <select onChange={handleNameChange} value={formData.conference} name="conference" id="conference" className="form-select" required>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                            return (
                            <option key={conference.href} value={conference.href}>
                                {conference.name}
                            </option>
                            );
                    })}
                </select>
              </div>
              <p className="mb-3">
                Now, tell us about yourself.
              </p>
              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input required onChange={handleNameChange} value={formData.name} placeholder="Your full name" type="text" id="name" name="name" className="form-control"></input>
                    <label htmlFor="name">Your full name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input required onChange={handleNameChange} value={formData.email} placeholder="Your email address" type="email" id="email" name="email" className="form-control"></input>
                    <label htmlFor="email">Your email address</label>
                  </div>
                </div>
              </div>
              <button className="btn btn-lg btn-primary">I'm going!</button>
            </form>
            {/* <div className="alert alert-success d-none mb-0" id="success-message">
              Congratulations! You're all signed up!
            </div> */}
          </div>
        </div>
      </div>
    </div>
  </div>
  )
};

export default AttendConferenceForm;
