import logo from './logo.svg';
import Nav from './Nav';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
  useParams,
} from 'react-router-dom'
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList'
import ConferenceForm from './ConferenceForm'
import AttendConferenceForm from './AttendConference'
import PresentationForm from './PresentationForm'
import MainPage from './MainPage'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="attendees">
              <Route path="new" element={<AttendConferenceForm />} />
            </Route>
            <Route path="presentations">
              <Route path="new" element={<PresentationForm />} />
            </Route>
          {/* <AttendeesList attendees={props.attendees}/> */}
          </Routes>
        </div>
      </BrowserRouter>
  );
}

export default App;
