import React, { useEffect, useState } from 'react';

function ConferenceForm () {
    const [locations, setLocations] = useState([]);

    const [formData, setFormData] = useState({
        name: "",
        starts: "",
        ends: "",
        description: "",
        max_presentations: "",
        max_attendees: "",
        location: "",
    })

    const handleNameChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setLocations(data.locations)
            }
        }

    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log(formData)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                const newConference = await response.json();

                setFormData({
                    name: "",
                    starts: "",
                    ends: "",
                    description: "",
                    max_presentations: "",
                    max_attendees: "",
                    location: "",
                })

            }
        };

    useEffect(() => {
        fetchData();
      }, []);

    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.starts} placeholder="Starts" required type="date" name="starts" id="start" className="form-control"></input>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.ends} placeholder="Ends" required type="date" name="ends" id="end" className="form-control"></input>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" value={formData.description} className="form-label">Description</label>
                <textarea onChange={handleNameChange} required type="text" className="form-control" name="description" id="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleNameChange} value={formData.location} required id="location" className="form-select" name="location">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                            return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                            );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
)};

export default ConferenceForm;
