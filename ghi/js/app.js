function createCard(name, description, pictureUrl, starts, ends, locationName) {
    return `
    <div class="col">
      <div class="card shadow-lg mb-5 bg-white rounded";>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">${starts}-${ends}</div>
      </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'; // Response
    try {
        const response = await fetch(url); // Catch

        if (!response.ok) {
            const errors = document.querySelector(".something")
            const html = `<div class="alert alert-danger" role="alert">Response is sad</div>`
            errors.innerHTML += html;
        } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                console.log(details)
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;

                const startDate = new Date(details.conference.starts);
                const starts = startDate.toLocaleDateString();
                const endDate = new Date(details.conference.ends);
                const ends = endDate.toLocaleDateString();

                const locationName = details.conference.location.name;

                const html = createCard(title, description, pictureUrl, starts, ends, locationName);

                const column = document.querySelector('.row');
                column.innerHTML += html;
            }
        }
        }
    }
    catch (e) {
        const errors = document.querySelector(".something")
        const html = `<div class="alert alert-danger" role="alert">I'm broken</div>`
        errors.innerHTML += html;
    }
});
